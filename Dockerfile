FROM python:3.6.4-slim

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN echo "hello" > /usr/src/app/index.html

CMD ["python", "-m", "http.server", "3333"]
